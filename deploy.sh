#!/usr/bin/env bash
echo Starting App server.

cd try_docker
echo "Collecting and compiling statics"
python manage.py collectstatic --noinput

exec gunicorn try_docker.wsgi:application \
    --bind 0.0.0.0:8000 \
    --workers 3