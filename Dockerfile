FROM python:3.6-alpine

RUN apk update && apk add bash

COPY requirements.txt requirements
COPY try_docker try_docker
RUN pip install  -r requirements \
    && rm -rf requirements

COPY deploy.sh /deploy.sh
RUN chmod +x /deploy.sh
EXPOSE 8000

CMD ["/deploy.sh"]



